<?php

namespace Drupal\example_promisepay_integration\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Payout portion entities.
 *
 * @ingroup example_promisepay_integration
 */
class PPPayoutPortionDeleteForm extends ContentEntityDeleteForm {


}
