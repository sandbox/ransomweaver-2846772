<?php

namespace Drupal\example_promisepay_integration\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Payout entities.
 *
 * @ingroup example_promisepay_integration
 */
class PPPayoutDeleteForm extends ContentEntityDeleteForm {


}
