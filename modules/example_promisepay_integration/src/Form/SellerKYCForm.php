<?php

namespace Drupal\example_promisepay_integration\Form;

use Drupal\commerce_promisepay\PromisePayAPI;
use Drupal\Core\Url;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\example_promisepay_integration\Entity\KYC;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class SellerKYCForm.
 *
 * @package Drupal\example_promisepay_integration\Form
 */
class SellerKYCForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'seller_kyc_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $user_id = \Drupal::routeMatch()->getParameter('user');
    if (empty($user_id)) throw new NotFoundHttpException();
    $current_user = \Drupal::currentUser();
    if ($user_id != $current_user->id()) {
      $roles = $current_user->getRoles(true);
      if (!in_array('administrator', $roles) && !in_array('subadmin', $roles)) {
        throw new AccessDeniedHttpException();
      }
    }

    $ids = \Drupal::entityQuery('kyc')
      ->condition('user_id', $user_id)
      ->execute();
    if ($ids) {
      $id = reset($ids);
      $kyc = KYC::load($id);
    } else {
      $kyc = KYC::create(['user_id' => $user_id]);
    }


    $form_state->set('form_display_kyc' , EntityFormDisplay::collectRenderDisplay($kyc, 'default'));
    $form_state->set('kyc', [$kyc]);
    $form['entity_kyc'] = [
      '#type' => 'details',
      '#title' => 'Personal Details',
      '#tree' => TRUE,
      '#parents' => ['entity_kyc'],
      '#weight' => -20,
      '#open' => TRUE,
    ];
    $form_state
      ->get('form_display_kyc')
      ->buildForm($kyc, $form['entity_kyc'], $form_state);

    $form['entity_kyc']['user_id']['#access'] = FALSE;

    // details to show account verification state

    $form['kyc_status'] = [
      '#type' => 'details',
      '#title' => 'Account Verification Status',
      '#tree' => TRUE,
      '#weight' => -15,
      '#open' => TRUE,
    ];

    switch ($kyc->getVerificationState()) {
      case KYC::VERIFICATION_STATE_NONE:
        $state = 'Not checking';
        break;
      case KYC::VERIFICATION_STATE_PENDING:
        $state = 'Pending, need more information from the seller.';
        break;
      case KYC::VERIFICATION_STATE_PENDING_CHECK:
        $state = 'Verification in process';
        break;
      case KYC::VERIFICATION_STATE_APPROVED_KYC_CHECK:
        $state = 'Approved, underwriting in progress';
        break;
      case KYC::VERIFICATION_STATE_APPROVED:
        $state = 'Approved to receive funds for sales on the platform';
        break;
      default:
        $state = '';
    }

    $form['kyc_status']['state'] = [
      '#markup' => '<p>' . $state . '</p>'
    ];

    $form['device_id'] = [
      '#type' => 'hidden',
      '#title' => $this->t('Device ID'),
    ];
    $form['ip_address'] = [
      '#type' => 'hidden',
      '#title' => $this->t('IP Address'),
    ];
    $form['bank_information'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Bank Account Information'),
    ];

    // if bank account info is already set, say so and provide button to remove existing bank info
    // TODO: needs a confirm screen

    $bank_account_id = $kyc->getBankAccountId();
    if (empty($bank_account_id)) {

      $form['#attributes']['id'][] = 'promisepay-bank-form';
      $form['#attributes']['autocomplete'] = 'on';
      if (PromisePayAPI::isTestEnvironment()) {
        $form['#attached']['library'][] = 'commerce_promisepay/promisepay_prelive';
        $environment = 'prelive';
      } else {
        $form['#attached']['library'][] = 'commerce_promisepay/promisepay_live';
        $environment = 'production';
      }
      $form['#attached']['drupalSettings']['promisepay'] = [
        'environment' => $environment,
      ];
      $form['#attached']['library'][] = 'commerce_promisepay/bank_form';

      $token = PromisePayAPI::generateCardToken($kyc->getOwner()->uuid(), true);
      if (empty($token['error'])) {
        $form['bank_information']['bank_token'] = [
          '#type' => 'hidden',
          '#attributes' => [
            'data-promisepay-bank-token' => $token['token'],
          ],
        ];
      } else {
        drupal_set_message($token['error'], 'error');
      }

      $form['bank_information']['user_id'] = [
        '#type' => 'hidden',
        '#default_value' => $kyc->getOwner()->uuid(),
        '#attributes' => [
          'data-promisepay-encrypted-name' => ['userId'],
        ],
        '#after_build' => [
          [get_class($this), 'removeNameAttribute'],
        ],
      ];
      $form['bank_information']['bank_name'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Bank Name'),
        '#maxlength' => 64,
        '#size' => 64,
        '#attributes' => [
          'data-promisepay-encrypted-name' => ['bankName'],
        ],
        '#after_build' => [
          [get_class($this), 'removeNameAttribute'],
        ],
      ];
      $form['bank_information']['account_name'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Account Holder Name'),
        '#maxlength' => 64,
        '#size' => 64,
        '#attributes' => [
          'data-promisepay-encrypted-name' => ['bankAccountName'],
        ],
        '#after_build' => [
          [get_class($this), 'removeNameAttribute'],
        ],
      ];
      $form['bank_information']['routing_number'] = [
        '#type' => 'tel',
        '#title' => $this->t('Routing Number'),
        '#maxlength' => 64,
        '#size' => 64,
        '#attributes' => [
          'data-promisepay-encrypted-name' => ['bankRoutingNumber'],
        ],
        '#after_build' => [
          [get_class($this), 'removeNameAttribute'],
        ],
      ];
      $form['bank_information']['account_number'] = [
        '#type' => 'tel',
        '#title' => $this->t('Account Number'),
        '#maxlength' => 64,
        '#size' => 64,
        '#attributes' => [
          'data-promisepay-encrypted-name' => ['bankAccountNumber'],
        ],
        '#after_build' => [
          [get_class($this), 'removeNameAttribute'],
        ],
      ];
      $form['bank_information']['account_type'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Account Type'),
        '#field_suffix' => $this->t('e.g. &quot;checking&quot;'),
        '#maxlength' => 64,
        '#size' => 64,
        '#attributes' => [
          'data-promisepay-encrypted-name' => ['bankAccountType'],
        ],
        '#after_build' => [
          [get_class($this), 'removeNameAttribute'],
        ],
      ];
      $form['bank_information']['holder_type'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Holder Type'),
        '#field_suffix' => $this->t('e.g. &quot;personal&quot;'),
        '#maxlength' => 64,
        '#size' => 64,
        '#attributes' => [
          'data-promisepay-encrypted-name' => ['bankHolderType'],
        ],
        '#after_build' => [
          [get_class($this), 'removeNameAttribute'],
        ],
      ];
      $form['bank_information']['country'] = [
        '#type' => 'hidden',
        '#title' => $this->t('Country'),
        '#default_value' => 'USA',
        '#attributes' => [
          'data-promisepay-encrypted-name' => ['bankCountry'],
        ],
        '#after_build' => [
          [get_class($this), 'removeNameAttribute'],
        ],
      ];
      $form['bank_information']['currency'] = [
        '#type' => 'hidden',
        '#title' => $this->t('Currency'),
        '#default_value' => 'USD',
        '#attributes' => [
          'data-promisepay-encrypted-name' => ['payoutCurrency'],
        ],
        '#after_build' => [
          [get_class($this), 'removeNameAttribute'],
        ],
      ];
      $form['bank_information']['pp_account_id'] = [
        '#type' => 'hidden',
        '#title' => $this->t('PromisePay account ID'),
      ];
      $form['bank_information']['error'] = [
        '#markup' => '<p id="promisepay-server-error" class="promisepay-server-error" style="display: none"></p>'
      ];
    } else {
      $form['bank_information']['is_set'] = [
        '#markup' => '<p>Bank account information is set</p>'
      ];
      $form['bank_information']['delete'] = array(
        '#type' => 'submit',
        '#value' => 'Delete Account Information',
        '#submit' => array('::removeBankInfo'),
      );
    }

    $form['submit_button'] = array(
      '#type' => 'submit',
      '#value' => 'Submit',
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\Core\Entity\Display\EntityFormDisplayInterface $form_display */
    $form_display = $form_state->get('form_display_kyc');
    if (isset($form['entity_kyc'])) {
      $kyc = $form_state->get('kyc')[0];
      $form_display->extractFormValues($kyc, $form['entity_kyc'], $form_state);
      $form_display->validateFormValues($kyc, $form['entity_kyc'], $form_state);
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // save the kyc entity
    $kyc = $form_state->get('kyc');
    if (!empty($kyc)) {
      $kyc = $kyc[0];
      $values = $form_state->getValues();
      $pp_account_id = $form_state->getValue('pp_account_id');
      if (!empty($pp_account_id)) {
        $kyc->setBankAccountId($pp_account_id);
        $response = PromisePayAPI::setDisbursementAccount($kyc->getOwner()->uuid(), $pp_account_id);
        if (!empty($response['error'])) {
          drupal_set_message($response['error'], 'error');
        }
      }
      $ip_address = $form_state->getValue('ip_address');
      if (!empty($ip_address)) {
        $kyc->setIpAddress($ip_address);
      }
      $device_id = $form_state->getValue('device_id');
      if (!empty($device_id)) {
        $kyc->setDeviceId($device_id);
      }
      $kyc->save();
    }
  }

  public function removeBankInfo(array &$form, FormStateInterface $form_state) {
    // save the kyc entity
    drupal_set_message('Banking information has been removed.');
    $kyc = $form_state->get('kyc');
    if (!empty($kyc)) {
      $kyc = $kyc[0];
      $kyc->setBankAccountId('');
      $kyc->setVerificationState(KYC::VERIFICATION_STATE_PENDING);
      $kyc->save();
    }

    $form_state->setRedirectUrl(Url::fromRoute('example_promisepay_integration.seller_kyc_form', ['user' => $kyc->getOwnerId()]));
  }

  public static function removeNameAttribute(array $element, FormStateInterface $form_state) {
    unset($element['#name']);
    return $element;
  }


}
