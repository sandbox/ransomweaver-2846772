<?php

namespace Drupal\example_promisepay_integration;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Know Your Customer entity.
 *
 * @see \Drupal\example_promisepay_integration\Entity\KYC.
 */
class KYCAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\example_promisepay_integration\Entity\KYCInterface $entity */
    switch ($operation) {
      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'view kyc entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit kyc entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete kyc entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add know your customer entities');
  }

}
