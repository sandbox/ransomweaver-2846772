<?php

/**
 * @file
 * Contains kyc.page.inc.
 *
 * Page callback for Know Your Customer entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Prepares variables for Know Your Customer templates.
 *
 * Default template: kyc.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_kyc(array &$variables) {
  // Fetch KYC Entity Object.
  $kyc = $variables['elements']['#kyc'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
