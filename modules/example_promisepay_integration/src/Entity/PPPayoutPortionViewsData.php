<?php

namespace Drupal\example_promisepay_integration\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Payout portion entities.
 */
class PPPayoutPortionViewsData extends EntityViewsData implements EntityViewsDataInterface {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['payout_portion']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('Payout portion'),
      'help' => $this->t('The Payout portion ID.'),
    );

    return $data;
  }

}
