<?php

namespace Drupal\example_promisepay_integration\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Payout entities.
 */
class PPPayoutViewsData extends EntityViewsData implements EntityViewsDataInterface {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['payout']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('Payout'),
      'help' => $this->t('The Payout ID.'),
    );

    return $data;
  }

}
