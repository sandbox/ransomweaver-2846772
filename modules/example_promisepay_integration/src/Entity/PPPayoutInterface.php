<?php

namespace Drupal\example_promisepay_integration\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;

/**
 * Provides an interface for defining Payout entities.
 *
 * @ingroup example_promisepay_integration
 */
interface PPPayoutInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Payout's order.
   *
   * @return \Drupal\commerce_order\Entity\OrderInterface
   */
  public function getOrder();

  /**
   * Sets the Payout order.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The Payout's order.
   *
   * @return \Drupal\example_promisepay_integration\Entity\PPPayoutInterface
   *   The called Payout entity.
   */
  public function setOrder(OrderInterface $order);

  /**
   * Gets the Payout's drupal commerce_payment entity.
   *
   * @return \Drupal\commerce_payment\Entity\PaymentInterface
   */
  public function getPayment();

  /**
   * Sets the Payout payment entity.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The Payout's drupal commerce_payment entity.
   *
   * @return \Drupal\example_promisepay_integration\Entity\PPPayoutInterface
   *   The called Payout entity.
   */
  public function setPayment(PaymentInterface $payment);

  /**
   * Gets the Payout portions.
   *
   * @return \Drupal\example_promisepay_integration\Entity\PPPayoutPortionInterface[]
   *   The Payout portions.
   */
  public function getPortions();

  /**
   * Adds a payout portion
   *
   * @param int $payee
   *   The Payout portion payee id.
   * @param int $amount
   *   The portion in cents
   * @param OrderInterface $order
   * @param string $item_id
   *   The remote item identifier
   *
   * @return \Drupal\example_promisepay_integration\Entity\PPPayoutInterface
   *   The called Payout entity.
   */
  public function addPortion($payee, $amount, OrderInterface $order, $item_id);

  /**
   * Gets the Payout platform percent.
   *
   * @return int
   *   The percentage taken by the platform.
   */
  public function getPlatformPercent();

  /**
   * Sets the Payout platform percent.
   *
   * @param int $percent
   *   The Payout platform percent.
   *
   * @return \Drupal\example_promisepay_integration\Entity\PPPayoutInterface
   *   The called Payout entity.
   */
  public function setPlatformPercent($percent);

  /**
   * Gets the Payout affiliate percent.
   *
   * @return int
   *   The percentage taken by the affiliate.
   */
  public function getAffiliatePercent();

  /**
   * Sets the Payout affiliate percent.
   *
   * @param int $percent
   *   The Payout affiliate percent.
   *
   * @return \Drupal\example_promisepay_integration\Entity\PPPayoutInterface
   *   The called Payout entity.
   */
  public function setAffiliatePercent($percent);

  /**
   * Gets the Payout gateway percent.
   *
   * @return float
   *   The percentage taken by the gateway.
   */
  public function getGatewayPercent();

  /**
   * Sets the Payout gateway percent.
   *
   * @param float $percent
   *   The Payout gateway percent.
   *
   * @return \Drupal\example_promisepay_integration\Entity\PPPayoutInterface
   *   The called Payout entity.
   */
  public function setGatewayPercent($percent);

  /**
   * Gets the Payout gateway transaction fee.
   *
   * @return int
   *   The percentage taken by the gateway.
   */
  public function getGatewayTransactionFee();

  /**
   * Sets the Payout gateway transaction fee.
   *
   * @param int $fee
   *   The Payout gateway transaction fee in cents.
   *
   * @return \Drupal\example_promisepay_integration\Entity\PPPayoutInterface
   *   The called Payout entity.
   */
  public function setGatewayTransactionFee($fee);

  /**
   * Gets the PromisePay Fee ID applied to collect the non-seller portion.
   *
   * @return string
   *   The Fee ID.
   */
  public function getSellFeeId();

  /**
   * Sets the PromisePay Fee ID applied to collect the non-seller portion.
   *
   * @param string $fee_id
   *   The Fee ID.
   *
   * @return \Drupal\example_promisepay_integration\Entity\PPPayoutInterface
   *   The called Payout entity.
   */
  public function setSellFeeId($fee_id);

  /**
   * Gets the seller portion
   *
   * @return int
   *   The seller portion in cents.
   */
  public function getSellerPortion();

  /**
   * Sets the seller portion
   *
   * @param int $amount
   *   The seller portion in cents.
   *
   * @return \Drupal\example_promisepay_integration\Entity\PPPayoutInterface
   *   The called Payout entity.
   */
  public function setSellerPortion($amount);

  /**
   * the (estimated) amount to be taken by the gateway for the transaction
   *
   * @return int
   *  the amount
   */
  public function getGatewayPortion();

  /**
   * sets the (estimated) amount to be taken by the gateway for the transaction
   *
   * @param int $amount
   * @return \Drupal\example_promisepay_integration\Entity\PPPayoutInterface
   *   The called Payout entity.
   */
  public function setGatewayPortion($amount);

  /**
   * Gets the platform net portion
   *
   * @return int
   *   The platform net portion in cents.
   */
  public function getPlatformNetPortion();

  /**
   * Sets the platform net portion
   *
   * @param int $amount
   *   The platform net portion in cents.
   *
   * @return \Drupal\example_promisepay_integration\Entity\PPPayoutInterface
   *   The called Payout entity.
   */
  public function setPlatformNetPortion($amount);


  /**
   * Gets the tax amount
   *
   * @return int
   *   The tax amount in cents.
   */
  public function getTaxCollected();

  /**
   * Sets the tax amount
   *
   * @param int $amount
   *   The tax amount in cents
   *
   * @return \Drupal\example_promisepay_integration\Entity\PPPayoutInterface
   *   The called Payout entity.
   */
  public function setTaxCollected($amount);

  /**
   * Gets the Payout creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Payout.
   */
  public function getCreatedTime();

  /**
   * Sets the Payout creation timestamp.
   *
   * @param int $timestamp
   *   The Payout creation timestamp.
   *
   * @return \Drupal\example_promisepay_integration\Entity\PPPayoutInterface
   *   The called Payout entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Payout complete status indicator.
   *
   *
   * @return bool
   *   TRUE if the Payout is complete.
   */
  public function isComplete();
  public function isPublished();

  /**
   * Sets the complete status of a Payout.
   *
   * @param bool $complete
   *   TRUE to set this Payout to complete, FALSE to set it to completed.
   *
   * @return \Drupal\example_promisepay_integration\Entity\PPPayoutInterface
   *   The called Payout entity.
   */
  public function setComplete($complete);
  public function setPublished($published);

  /**
   * Calculate payouts from items in the order
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   * @param integer $amount
   */
  public function calculatePayouts(OrderInterface $order, $amount);

  /**
   * create the fee on PromisePay and return its id
   * @param int $payment_amount
   *  the total amount of the payment
   * @return string fee
   */
  public function createFee($payment_amount);

}
