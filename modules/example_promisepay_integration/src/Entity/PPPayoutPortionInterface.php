<?php

namespace Drupal\example_promisepay_integration\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Payout portion entities.
 *
 * @ingroup example_promisepay_integration
 */
interface PPPayoutPortionInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  /**
   * Gets the Payout portion's order.
   *
   * @return \Drupal\commerce_order\Entity\Order
   */
  public function getOrder();

  /**
   * Sets the Payout portion's order.
   *
   * @param \Drupal\commerce_order\Entity\Order $order
   *   The Payout's order.
   *
   * @return \Drupal\example_promisepay_integration\Entity\PPPayoutPortionInterface
   *   The called Payoutportion  entity.
   */
  public function setOrder($order);

  /**
   * Gets the Payout portion creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Payout portion.
   */
  public function getCreatedTime();

  /**
   * Sets the Payout portion creation timestamp.
   *
   * @param int $timestamp
   *   The Payout portion creation timestamp.
   *
   * @return \Drupal\example_promisepay_integration\Entity\PPPayoutPortionInterface
   *   The called Payout portion entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the Payout portion amount.
   *
   * @return int
   *   Amount the Payout portion.
   */
  public function getAmount();

  /**
   * Sets the Payout portion amount.
   *
   * @param int $amount
   *   The Payout portion amount.
   *
   * @return \Drupal\example_promisepay_integration\Entity\PPPayoutPortionInterface
   *   The called Payout portion entity.
   */
  public function setAmount($amount);

  /**
   * Gets the Payout portion remote item id.
   *
   * @return string
   *   Remote Item ID paying the Payout portion.
   */
  public function getRemoteItemId();

  /**
   * Sets the Payout portion remote item id.
   *
   * @param string $item_id
   *   The Payout portion remote item id.
   *
   * @return \Drupal\example_promisepay_integration\Entity\PPPayoutPortionInterface
   *   The called Payout portion entity.
   */
  public function setRemoteItemId($item_id);

  /**
   * Returns the Payout portion Paid status indicator.
   *
   * @return bool
   *   TRUE if the Payout portion is paid.
   */
  public function isPaid();
  public function isPublished();

  /**
   * Sets the paid status of a Payout portion.
   *
   * @param bool $paid
   *   TRUE to set this Payout portion to paid, FALSE to set it to not paid.
   *
   * @return \Drupal\example_promisepay_integration\Entity\PPPayoutPortionInterface
   *   The called Payout portion entity.
   */
  public function setPaid($paid);
  public function setPublished($published);

}
