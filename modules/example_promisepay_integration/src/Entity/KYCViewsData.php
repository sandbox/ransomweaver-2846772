<?php

namespace Drupal\example_promisepay_integration\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Know Your Customer entities.
 */
class KYCViewsData extends EntityViewsData implements EntityViewsDataInterface {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['kyc']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('Know Your Customer'),
      'help' => $this->t('The Know Your Customer ID.'),
    );

    return $data;
  }

}
