<?php

namespace Drupal\example_promisepay_integration\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\address\AddressInterface;

/**
 * Provides an interface for defining Know Your Customer entities.
 *
 * @ingroup example_promisepay_integration
 */
interface KYCInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Know Your Customer first name.
   *
   * @return string
   *   First Name of the Know Your Customer.
   */
  public function getFirstName();

  /**
   * Sets the Know Your Customer first name.
   *
   * @param string $name
   *   The Know Your Customer first name.
   *
   * @return \Drupal\example_promisepay_integration\Entity\KYCInterface
   *   The called Know Your Customer entity.
   */
  public function setFirstName($name);

  /**
   * Gets the Know Your Customer last name.
   *
   * @return string
   *   Last Name of the Know Your Customer.
   */
  public function getLastName();

  /**
   * Sets the Know Your Customer first name.
   *
   * @param string $name
   *   The Know Your Customer last name.
   *
   * @return \Drupal\example_promisepay_integration\Entity\KYCInterface
   *   The called Know Your Customer entity.
   */
  public function setLastName($name);

  /**
   * Gets the Know Your Customer mobile phone.
   *
   * @return string
   *   Mobile of the Know Your Customer.
   */
  public function getMobile();

  /**
   * Sets the Know Your Customer mobile phone number.
   *
   * @param string $mobile
   *   The Know Your Customer mobile phone.
   *
   * @return \Drupal\example_promisepay_integration\Entity\KYCInterface
   *   The called Know Your Customer entity.
   */
  public function setMobile($mobile);

  /**
   * gets the Date of birth
   *
   * @return string
   *   Datetime format date like 1970-01-20
   */
  public function getDateOfBirth();

  /**
   * gets the Date of birth as required by PromisePay
   *
   * @return string
   *   Formatted date like 20/01/1970
   */
  public function getPPDateOfBirth();

  /**
   * gets the government id, which should be obfuscated
   *
   * @return string
   */
  public function getGovernmentId();

  /**
   * Sets the Know Your Customer ssn, in obfuscated format
   *
   * @param string $id
   *
   * @return \Drupal\example_promisepay_integration\Entity\KYCInterface
   *   The called Know Your Customer entity.
   */
  public function setGovernmentId($id);

  /**
   * gets the PromisePay verification state
   *
   * @return string
   */
  public function getVerificationState();

  /**
   * Sets the PromisePay verification state
   *
   * @param string $state
   *
   * @return \Drupal\example_promisepay_integration\Entity\KYCInterface
   *   The called Know Your Customer entity.
   */
  public function setVerificationState($state);

  /**
   * gets the PromisePay bank account ID
   *
   * @return string
   */
  public function getBankAccountId();

  /**
   * Sets the PromisePay bank account id
   *
   * @param string $id
   *
   * @return \Drupal\example_promisepay_integration\Entity\KYCInterface
   *   The called Know Your Customer entity.
   */
  public function setBankAccountId($id);

  /**
   * Sets the Know Your Customer device id.
   *
   * @param string $device_id
   *   The Know Your Customer device id, by appending.
   *
   * @return \Drupal\example_promisepay_integration\Entity\KYCInterface
   *   The called Know Your Customer entity.
   */
  public function setDeviceId($device_id);

  /**
   * Sets the Know Your Customer ip address.
   *
   * @param string $ip
   *   The Know Your Customer ip address, by appending
   *
   * @return \Drupal\example_promisepay_integration\Entity\KYCInterface
   *   The called Know Your Customer entity.
   */
  public function setIPAddress($ip);

  /**
   * Gets the Know Your Customer email.
   *
   * @return string
   *   Email of the Know Your Customer.
   */
  public function getEmail();

  /**
   * Gets the Business Address associated with the Retail Store.
   *
   * @return \Drupal\address\AddressInterface
   *    The Business Address associated with the Retail Store.
   */
  public function getAddress();

  /**
   * Sets the Business Address associated with the Retail Store.
   *
   * @param \Drupal\address\AddressInterface $address
   *    The Business Address associated with the Retail Store.
   *
   * @return \Drupal\example_retail_store\RetailStoreInterface
   *    The called Retail Store entity.
   */
  public function setAddress(AddressInterface $address);

  /**
   * Gets the Know Your Customer creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Know Your Customer.
   */
  public function getCreatedTime();

  /**
   * Sets the Know Your Customer creation timestamp.
   *
   * @param int $timestamp
   *   The Know Your Customer creation timestamp.
   *
   * @return \Drupal\example_promisepay_integration\Entity\KYCInterface
   *   The called Know Your Customer entity.
   */
  public function setCreatedTime($timestamp);



}
