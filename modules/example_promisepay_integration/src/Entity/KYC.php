<?php

namespace Drupal\example_promisepay_integration\Entity;

use Drupal\address\AddressInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;
use CommerceGuys\Addressing\AddressFormat\AddressField;
use Drupal\commerce_promisepay\PromisePayAPI;

/**
 * Defines the Know Your Customer entity.
 *
 * @ingroup example_promisepay_integration
 *
 * @ContentEntityType(
 *   id = "kyc",
 *   label = @Translation("Know Your Customer"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\example_promisepay_integration\KYCListBuilder",
 *     "views_data" = "Drupal\example_promisepay_integration\Entity\KYCViewsData",
 *     "translation" = "Drupal\example_promisepay_integration\KYCTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\example_promisepay_integration\Form\KYCForm",
 *       "add" = "Drupal\example_promisepay_integration\Form\KYCForm",
 *       "edit" = "Drupal\example_promisepay_integration\Form\KYCForm",
 *       "delete" = "Drupal\example_promisepay_integration\Form\KYCDeleteForm",
 *     },
 *     "access" = "Drupal\example_promisepay_integration\KYCAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\example_promisepay_integration\KYCHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "kyc",
 *   data_table = "kyc_field_data",
 *   translatable = TRUE,
  *   admin_permission = "administer know your customer entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "last_name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/kyc/{kyc}",
 *     "add-form" = "/admin/structure/kyc/add",
 *     "edit-form" = "/admin/structure/kyc/{kyc}/edit",
 *     "delete-form" = "/admin/structure/kyc/{kyc}/delete",
 *     "collection" = "/admin/structure/kyc",
 *   },
 *   field_ui_base_route = "kyc.settings"
 * )
 */
class KYC extends ContentEntityBase implements KYCInterface {

  use EntityChangedTrait;

  const VERIFICATION_STATE_NONE = '';
  const VERIFICATION_STATE_PENDING = 'pending';
  const VERIFICATION_STATE_PENDING_CHECK = 'pending_check';
  const VERIFICATION_STATE_APPROVED_KYC_CHECK = 'approved_kyc_check';
  const VERIFICATION_STATE_APPROVED = 'approved';

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $user_id = \Drupal::routeMatch()->getParameter('user');
    if (empty($user_id)) {
      $current_user = \Drupal::currentUser();
      $user_id = $current_user->id();
    }
    $values += array(
      'user_id' => $user_id,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFirstName() {
    return $this->get('first_name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setFirstName($first_name) {
    $this->set('first_name', $first_name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getLastName() {
    return $this->get('last_name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setLastName($last_name) {
    $this->set('last_name', $last_name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getMobile() {
    return $this->get('mobile')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setMobile($mobile) {
    $this->set('mobile', $mobile);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setDeviceId($device_id) {
    $this->get('device_id')->appendItem($device_id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setIPAddress($ip) {
    $this->get('ip_address')->appendItem($ip);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getEmail() {
    return $this->get('user_id')->entity->getEmail();
  }

  /**
   * {@inheritdoc}
   */
  public function getAddress() {
    return $this->get('address')->first();
  }

  /**
   * {@inheritdoc}
   */
  public function setAddress(AddressInterface $address) {
    // $this->set('address', $address) results in the address being appended
    // to the item list, instead of replacing the existing first item.
    $this->address = $address;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDateOfBirth() {
    return $this->get('date_of_birth')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getPPDateOfBirth() {
    $datetime_format = 'Y-m-d';
    $pp_format = 'd/m/Y';
    $date = date_create_from_format($datetime_format, $this->get('date_of_birth')->value);
    return date_format($date, $pp_format);
  }

  /**
   * {@inheritdoc}
   */
  public function getGovernmentId() {
    return $this->get('government_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setGovernmentId($id) {
    $id = str_repeat('*', strlen($id) - 4) . substr($id, -4);
    $this->set('government_id', $id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getVerificationState() {
    return $this->get('verification_state')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setVerificationState($state) {
    $this->set('verification_state', $state);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getBankAccountId() {
    return $this->get('bank_account_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setBankAccountId($id) {
    $this->set('bank_account_id', $id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Know Your Customer entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', array(
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => array(
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ),
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['first_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('First Name'))
      ->setRequired(TRUE)
      ->setSettings(array(
        'max_length' => 50,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => 0,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => 0,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['last_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Last Name'))
      ->setRequired(TRUE)
      ->setSettings(array(
        'max_length' => 50,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => 1,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => 1,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['mobile'] = BaseFieldDefinition::create('telephone')
      ->setLabel(t('Mobile Phone'))
      ->setDisplayOptions('view', array(
        'label' => 'inline',
        'type' => 'string',
        'weight' => 2,
      ))
      ->setDisplayOptions('form', [
        'type' => 'telephone_default',
        'weight' => 2,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $disabled_fields = array(
      AddressField::FAMILY_NAME,
      AddressField::ADDITIONAL_NAME,
      AddressField::GIVEN_NAME,
      AddressField::ORGANIZATION);              // Disable the recipient and organization field on the retail store address.

    $fields['address'] = BaseFieldDefinition::create('address')
      ->setLabel(t('Personal Address'))
      ->setDescription(t('Your address, for credit purposes.'))
      ->setCardinality(1)
      ->setRequired(TRUE)
      ->setSetting('fields', array_diff(AddressField::getAll(), $disabled_fields))
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => 3,
      ))
      ->setDisplayOptions('form', [
        'type' => 'address_default',
        'settings' => [
          'default_country' => 'site_default',
        ],
        'weight' => 3,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['date_of_birth'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('Date of Birth'))
      ->setSetting('datetime_type', 'date')
      ->setRequired(true)
      ->setDisplayOptions('form', array(
        'type' => 'date',
        'weight' => 4,
      ))
      ->setDisplayOptions('view', array(
        'label' => 'inline',
        'type' => 'string',
        'weight' => 4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['government_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Social Security Number'))
      ->setSettings(array(
        'max_length' => 20,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => 5,
      ))

      ->setDisplayOptions('view', array(
        'label' => 'inline',
        'type' => 'string',
        'weight' => 5,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['verification_state'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Verification State'))
      ->setSettings(array(
        'max_length' => 50,
        'text_processing' => 0,
      ))

      ->setDisplayOptions('view', array(
        'label' => 'inline',
        'type' => 'string',
        'weight' => 7,
      ))
      ->setDefaultValue(self::VERIFICATION_STATE_NONE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['bank_account_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Bank Account ID'))
      ->setSettings(array(
        'max_length' => 50,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayConfigurable('view', TRUE);

    $fields['device_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Mobile device ID'))
      ->setSettings(array(
        'max_length' => 2000,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setDisplayConfigurable('view', TRUE);

    $fields['ip_address'] = BaseFieldDefinition::create('string')
      ->setLabel(t('IP address'))
      ->setSettings(array(
        'max_length' => 50,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setDisplayConfigurable('view', TRUE);

    $fields['held_state'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Fraud state'))
      ->setDefaultValue(FALSE)
      ->setDescription(t('Is the user considered fraudulent'));

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);
    // update the user on promisepay and set the user verification status
    $updates = [
      'first_name' => $this->getFirstName(),
      'last_name' => $this->getLastName(),
      'dob' => $this->getPPDateOfBirth(),
      'address_line1' => $this->getAddress()->getAddressLine1(),
      'address_line2' => $this->getAddress()->getAddressLine2(),
      'city' => $this->getAddress()->getLocality(),
      'state' => $this->getAddress()->getAdministrativeArea(),
      'zip' => $this->getAddress()->getPostalCode(),
      'country' => $this->getAddress()->getCountryCode()
    ];
    $mobile = $this->getMobile();
    if (!empty($mobile)) {
      if (strpos($mobile, '+1') !== 0) {
        $mobile = '+1' . $mobile;
        $this->setMobile($mobile);
      }
      $updates['mobile'] = $mobile;
    }
    $ssn = $this->getGovernmentId();
    if (!empty($ssn) && strpos($ssn, '*') === false) {
      $updates['government_number'] = $ssn;
      $this->setGovernmentId($ssn); // obfuscates it before saving
    }
    $response = PromisePayAPI::updateUser($this->getOwner()->uuid(), $updates);
    $err = $response['error'];
    if (!empty($err)) {
      $msg = t('User %first %last could not be updated on PromisePay because: %err', ['%first' => $this->getFirstName(), '%last' => $this->getLastName(), '%err' => $err]);
      drupal_set_message($msg, 'error');
      \Drupal::logger('promisepay_integration')->info($msg);
    } else {
      $verification_state = $response['verification_state'];
      if ($verification_state) {
        $this->setVerificationState($verification_state);
      }
    }

  }

}
