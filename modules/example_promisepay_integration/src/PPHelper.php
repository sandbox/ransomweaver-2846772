<?php

namespace Drupal\example_promisepay_integration;

use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\user\Entity\User;
use Drupal\commerce_promisepay\PromisePayAPI;
use Drupal\example_promisepay_integration\Entity\KYC;
use Drupal\example_promisepay_integration\Entity\PPPayoutInterface;

/**
 * Provides helper methods
 */
class PPHelper {

  /**
   * Make payouts on Promisepay post-release of primary payment
   *
   * @param \Drupal\example_promisepay_integration\Entity\PPPayoutInterface $payout
   */
  public static function releasePayouts(PPPayoutInterface $payout) {
    // seller payout has already happened; their payout is refrenced by its item uuid in the payout portions
    $seller_portion_id = $payout->getPayment()->getRemoteId();
    $order_number = $payout->getOrder()->getOrderNumber();
    $portions = $payout->getPortions();
    $pp_config = \Drupal::config('commerce_payment.commerce_payment_gateway.promisepay')->get('configuration');
    foreach ($portions as $portion) {
      if ($portion->getRemoteItemId() == $seller_portion_id) {
        $portion->setPaid(true);
        $portion->save();
      } else if ($portion->getRemoteItemId() == NULL) {
        // buyer is the platform
        // seller is the owner
        $seller = $portion->getOwner();
        // item is express
        $item = [
          'id' => $portion->uuid(),
          'name' => 'Portion for order #' . $order_number,
          'amount' => $portion->getAmount(),
          'payment_type' => 2,
          'buyer_id' => $pp_config['api_user_uuid'],
          'seller_id' => $seller->uuid(),
          'fee_ids' => '',
        ];
        $item_response = PromisePayAPI::createItem($item);
        if (isset($item_response['error'])) {
          \Drupal::logger('promisepay_integration')->error('payout portion item not created: ' . $item_response['error']);
        } else {
          $payment_response = PromisePayAPI::makePayment(
            $portion->uuid(),
            $pp_config['api_wallet_uuid']
          );
          if (isset($payment_response['error'])) {
            \Drupal::logger('promisepay_integration')->error('payout portion payment not processed: ' . $payment_response['error']);
          } else {
            $portion->setRemoteItemId($portion->uuid());
            $portion->setPaid(TRUE);
            $portion->save();
            $has_bank = false;
            $gateway_mode = $pp_config['mode'];
            if ($gateway_mode == 'production') {
              $kycs = \Drupal::entityQuery('kyc')
                ->condition('user_id', $seller->id())
                ->execute();
              if ($kycs) {
                $kyc_id = reset($kycs);
                $kyc = KYC::load($kyc_id);
                $bank_id = $kyc->getBankAccountId();
                if ($bank_id) {
                  $has_bank = TRUE;
                }
              }
              if (!$has_bank) {
                // tell the user that there is money for them andthey need to enter seller info to get paid
              }
            }
          }
        }
      }
    }
  }


}
