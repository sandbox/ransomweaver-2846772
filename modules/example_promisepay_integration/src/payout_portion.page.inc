<?php

/**
 * @file
 * Contains payout_portion.page.inc.
 *
 * Page callback for Payout portion entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Prepares variables for Payout portion templates.
 *
 * Default template: payout_portion.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_payout_portion(array &$variables) {
  // Fetch PPPayoutPortion Entity Object.
  $payout_portion = $variables['elements']['#payout_portion'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
