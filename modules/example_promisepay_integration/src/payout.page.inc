<?php

/**
 * @file
 * Contains payout.page.inc.
 *
 * Page callback for Payout entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Prepares variables for Payout templates.
 *
 * Default template: payout.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_payout(array &$variables) {
  // Fetch PPPayout Entity Object.
  $payout = $variables['elements']['#payout'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
