<?php

namespace Drupal\example_promisepay_integration;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for kyc.
 */
class KYCTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
