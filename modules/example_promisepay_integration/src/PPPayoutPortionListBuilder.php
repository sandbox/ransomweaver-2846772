<?php

namespace Drupal\example_promisepay_integration;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Routing\LinkGeneratorTrait;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of Payout portion entities.
 *
 * @ingroup example_promisepay_integration
 */
class PPPayoutPortionListBuilder extends EntityListBuilder {

  use LinkGeneratorTrait;

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Payout portion ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\example_promisepay_integration\Entity\PPPayoutPortion */
    $row['id'] = $entity->id();
    $row['name'] = $this->l(
      $entity->label(),
      new Url(
        'entity.payout_portion.edit_form', array(
          'payout_portion' => $entity->id(),
        )
      )
    );
    return $row + parent::buildRow($entity);
  }

}
