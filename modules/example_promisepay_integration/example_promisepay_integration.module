<?php

/**
 * @file
 * Contains example_promisepay_integration.module..
 */

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Exception\HardDeclineException;
use Drupal\commerce_promisepay\PromisePayAPI;
use Drupal\commerce_promisepay\Plugin\Commerce\PaymentGateway\OnsiteInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\example_retail_store\Entity\RetailStore;
use Drupal\commerce_order\Entity\Order;
use Drupal\example_promisepay_integration\Entity\PPPayout;
use Drupal\user\Entity\User;

/**
 * Implements hook_help().
 */
function example_promisepay_integration_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the example_promisepay_integration module.
    case 'help.page.example_promisepay_integration':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Integration example for commerce_promisepay and your platform') . '</p>';
      return $output;

    default:
  }
}


/**
 * Implements hook_entity_base_field_info().
 */

function example_promisepay_integration_entity_base_field_info(EntityTypeInterface $entity_type) {
  if ($entity_type->id() === 'commerce_order') {

  // add some base fields to track the users (affiliate?) who should get a payout besides the
  // "Seller" who is the owner of the product

    // add a field to hold the payouts for the order
    $fields['payouts'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Payouts'))
      ->setDescription(t('Payouts calculated for the order'))
      ->setSetting('target_type', 'payout')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'entity_reference_entity_view',
        'weight' => 3,
        'settings' => [
          'link' => true,
          'view_mode' => 'default'
        ],
      ))
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);


    return $fields;
  }
}

/**
 * Implements hook_cron().
 * Look for shipped products that have been shipped state for longer than the return period
 * and transition them to completed, which will trigger the payout
 */
function example_promisepay_integration_cron() {
  // TODO: this requires commerce_shipping, because we will check the order shipment ship date (set when a tracking code is set?)
 // $order_ready_to_complete = \Drupal::entityQuery('commerce_order')
 //   ->condition('state', 'shipped')
 //   ->condition()
}

/**
 * Implements hook_ENTITY_TYPE_insert().
 */
function example_promisepay_integration_user_insert(\Drupal\Core\Entity\EntityInterface $entity) {
  example_promisepay_integration_add_user($entity);
}

/**
 * Implements hook_ENTITY_TYPE_update().
 */
function example_promisepay_integration_user_update(\Drupal\Core\Entity\EntityInterface $entity) {
  example_promisepay_integration_update_user($entity);
}

/**
* create a user on promisepay from a new user on the platform
*/
function example_promisepay_integration_add_user($entity) {
  if (empty($entity->getEmail())) {
    // default_content doesn't store the user's email, so let's be clever and get it from promisepay, if available
    $res = PromisePayAPI::showUser($entity->uuid());
    if (empty($res['error'])) {
      $email = $res['email'];
      try {
        $entity->set('mail', $email);
        $entity->save();
      } catch (EntityStorageException $e) {
        \Drupal::logger('promisepay_integration')->info(t('default user %username failed to have %email set as it is already in use by an existing platform user', ['%username' => $entity->getUsername(), '%email' => $email]));
      }
    }
    return;
  }
  $response = PromisePayAPI::createUserFromUser($entity);
  if ($response && isset($response['error'])) {
    $error = $response['error'];
    if ($error == 'user_email_exists') {
      // Platform has a user with this email, but not with this uuid
      // this could result from deleting a user and adding it back, or the user might
      // have been from a dev installation of the site
      // we will just use the uuid that's on PromisePay for this NEW user
      // if somehow the uuid is a duplicate it will fail and log the error
      $promisepay_user = $response['promisepay_user'];
      $uuid = $promisepay_user['id'];
      if (!empty($uuid)) {
        try {
          $entity->set('uuid', $uuid);
          $entity->save();
        } catch (EntityStorageException $e) {
          \Drupal::logger('promisepay_integration')->info(t('user %username could not be created on PromisePay since %email is already in use by an existing platform user', ['%username' => $entity->getUsername(), '%email' => $entity->getEmail()]));
        }

      }
    } else {
      \Drupal::logger('promisepay_integration')->info(t('user %username could not be created on PromisePay with email %email.', ['%username' => $entity->getUsername(), '%email' => $entity->getEmail()]));
    }
  }
}


/**
 * update a user on promisepay from the drupal user account (not kyc or customer cc account)
 */
function example_promisepay_integration_update_user($entity) {
  $res = PromisePayAPI::showUser($entity->uuid());
  if (!empty($res['error'])) {
    \Drupal::logger('promisepay_integration')->info(t('No access to user %username on promisepay or it does not exist. Trying to create now.', ['%username' => $entity->getUsername()]));
    example_promisepay_integration_add_user($entity);
    return;
  }
  $update_values = [];
  if (empty($res['last_name'])) {
    // this user has not had a kyc/billing update the PP user with extra info, so save everything from the Drupal user account
    $update_values['email'] = $entity->getEmail();
    $update_values['first_name'] = $entity->getUsername();
  } else {
    // this user should get updated from KYC or billing, so only update the email
    $update_values['email'] = $entity->getEmail();
  }
  $response = PromisePayAPI::updateUser($entity->uuid(), $update_values);
  if ($response && isset($response['error'])) {
    $error = $response['error'];
    drupal_set_message($error, 'error');
  }
}

/**
 * Implements hook_promisepay_process_payment().
 */
function example_promisepay_integration_promisepay_process_payment(PaymentInterface &$payment, PaymentMethodInterface $payment_method, $capture, OnsiteInterface $plugin) {
  // Perform the create payment request here, throw an exception if it fails.
  // See \Drupal\commerce_payment\Exception for the available exceptions.
  // Remember to take into account $capture when performing the request.
  $amount = $payment->getAmount();
  $amount_cents = intval(floatval($amount->getNumber()) * 100);
  $payment_method_id = $payment_method->getRemoteId();
  $order = $payment->getOrder();

  $payout = PPPayout::create([
    'order_id' => $order->id(),
    'user_id' => 1, // payout objects are owned by the platform
    'gateway_percent' => $plugin->getGatewayPercentage(),
    'platform_transaction_fee' => $plugin->getGatewayTransactionFee(),
  ]);
  $payout->calculatePayouts($order, $amount_cents);
  $fee = $payout->createFee($amount_cents);

  // TODO: need a field on the payment entity telling us if its escrow or express, or what. Escrow will be default
  $payment_type_id = 1;  // 1 = Escrow, 2 = Express, 3 = Escrow Partial Release, 4 = Approve
  // create value array for PromisePay createItem call
  $values = [
    'id' => $payment->uuid(),
    'name' => $order->getStore()->getName() . ' Order #' . $order->getOrderNumber(),
    'amount' => $amount_cents,
    'payment_type' => $payment_type_id,
    'fee_ids' => $fee,
    'buyer_id' => $order->getCustomer()->uuid(),
    'seller_id' => $order->getStore()->getOwner()->uuid(),
    'description' => '',
    'custom_descriptor' => $order->getStore()->getName()
  ];
  // create item
  $itemResponse = PromisePayAPI::createItem($values);
  if (isset($itemResponse['error'])) {
    \Drupal::logger('promisepay_integration')->error($itemResponse['error']);
    throw new HardDeclineException($itemResponse['error']);
  }

  $payment->setRemoteState($itemResponse['state']);
  $payment->state = PromisePayAPI::itemState($itemResponse['status']);
  $payment->setAuthorizedTime(REQUEST_TIME);

  // $capture for PromisePay means makePayment on the Item to the seller at the time of order completion
  // for now it will immediately charge the user's payment method (card) and put the money into escrow
  // later (after the return period) we need to release the payment (which pays the seller and collects the fee)
  if ($capture) {
    $paymentResponse = PromisePayAPI::makePayment(
      $payment->uuid(),
      $payment_method_id,
      $payment_method->get('ip_address')->value,
      $payment_method->get('device_id')->value
    );
    if (isset($paymentResponse['error'])) {
      \Drupal::logger('promisepay_integration')->error('payment not processed: ' . $paymentResponse['error']);
      throw new HardDeclineException(t('The payment method could not be processed. Try another card or contact support.'));
    } else {
      $payment->setRemoteState($paymentResponse['state']);
      $payment->state = PromisePayAPI::itemState($paymentResponse['status']);
      $payment->setCapturedTime(REQUEST_TIME);
    }
  }

  $payment->setTest($plugin->getMode() == 'test');
  $payment->setRemoteId($itemResponse['id']);
  $payment->save();
  $payout->addPortion($payout->sellerId, $payout->getSellerPortion(), $order, $itemResponse['id']);
  $payout->setPayment($payment);
  $payout->save();
  $order->payouts->appendItem($payout);
  $order->save();
  return true;
}

function example_promisepay_integration_promisepay_process_refund(PaymentInterface &$payment, $item_id, $amount) {
  $order = $payment->getOrder();
  $refund_message = $order->getData('refund_message'); // optional refund message from your UI. could be null

    $refundResponse = PromisePayAPI::refund(
      $item_id,
      $amount,
      $refund_message
    );
    if ($refundResponse['error']) {
      drupal_set_message($refundResponse['error'], 'error');
      return false;
    } else {
      // your custom logic
    }

  return true;
}

