<?php

namespace Drupal\commerce_promisepay\PluginForm\Onsite;

use Drupal\commerce_payment\PluginForm\PaymentMethodAddForm as BasePaymentMethodAddForm;
use Drupal\Core\Form\FormStateInterface;

class PaymentMethodAddForm extends BasePaymentMethodAddForm {

  /**
   * {@inheritdoc}
   */
  protected function buildCreditCardForm(array $element, FormStateInterface $form_state) {
    /** @var \Drupal\commerce_promisepay\Plugin\Commerce\PaymentGateway\OnsiteInterface $plugin */
    $plugin = $this->plugin;

    // promisepay.js will collect
    // full_name, card_number, card_expiry, cvv and send direct to create a card account on PromisePay
    //
    // Onsite createPaymentMethod will process
    // address (from formstate billing_information), mobile number, ip address, device id, first last name
    // and update user on PromisePay via PHP API
    // and ip address, device id on PaymentMethod extra fields

    if ($plugin->getMode() == 'test') {
      $environment = 'prelive';
    } else {
      $environment = 'production';
    }
    $element['#attributes']['class'][] = 'promisepay-card-form';
    $element['#attributes']['autocomplete'] = 'on';

    $token = $plugin->generateCardToken();
    $element['#attached']['drupalSettings']['promisepay'] = [
      'environment' => $environment,
      'token'       => $token
    ];
    $element['#attached']['library'][] = 'commerce_promisepay/payment_form';

    $element['card_number'] = [
      '#type' => 'tel',
      '#title' => t('Card Number'),
      '#maxlength' => 20,
      '#size' => 27,
      '#attributes' => [
        'autocomplete' => ['cc-number'],
        'id' => 'cc-number',
        'placeholder' => '•••• •••• •••• ••••'
      ],
      '#after_build' => [
        [get_class($this), 'removeNameAttribute'],
      ],
    ];
    $element['card_deets'] = [
      '#type' => 'container',
    ];
    $element['card_deets']['card_expiry'] = [
      '#type' => 'tel',
      '#title' => t('Expiration'),
      '#maxlength' => 7,
      '#size' => 6  ,
      '#attributes' => [
        'autocomplete' => ['cc-exp'],
        'id' => 'cc-exp',
        'placeholder' => '•• / ••'
      ],
      '#after_build' => [
        [get_class($this), 'removeNameAttribute'],
      ],
    ];
    $element['card_deets']['cvv'] = [
      '#type' => 'tel',
      '#title' => t('CVV'),
      '#maxlength' => 4,
      '#size' => 4,
      '#attributes' => [
        'autocomplete' => ['off'],
        'id' => 'cc-cvc',
        'placeholder' => '•••'
      ],
      '#after_build' => [
        [get_class($this), 'removeNameAttribute'],
      ],
    ];

    $element['mobile'] = [
      '#type' => 'tel',
      '#title' => t('Mobile Phone'),
      '#maxlength' => 20,
      '#size' => 27,
    ];

    $element['device_id'] = [
      '#type' => 'hidden',
      '#attributes' => [
        'id' => 'device-id',
      ],
    ];

    $element['ip_address'] = [
      '#type' => 'hidden',
      '#attributes' => [
        'id' => 'ip-address',
      ],
    ];

    $element['last_4'] = [
      '#type' => 'hidden',
      '#attributes' => [
        'id' => 'last-4',
      ],
    ];

    $element['expiry_month'] = [
      '#type' => 'hidden',
      '#attributes' => [
        'id' => 'expiry-month',
      ],
    ];

    $element['expiry_year'] = [
      '#type' => 'hidden',
      '#attributes' => [
        'id' => 'expiry-year',
      ],
    ];

    $element['card_type'] = [
      '#type' => 'hidden',
      '#attributes' => [
        'id' => 'cc-type',
      ],
    ];

    $element['pp_account_id'] = [
      '#type' => 'hidden',
      '#attributes' => [
        'id' => 'pp-account-id',
      ],
    ];


    $element['error'] = [
      '#markup' => '<p id="promisepay-server-error" class="promisepay-server-error"></p>'
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  protected function validateCreditCardForm(array &$element, FormStateInterface $form_state) {
    // The JS library performs its own validation. This method prevents the superclass method execution
  }

  /**
   * {@inheritdoc}
   */
  public function submitCreditCardForm(array $element, FormStateInterface $form_state) {
    // The payment gateway plugin will process the submitted payment details. This prevents the superclass method
  }


  public static function removeNameAttribute(array $element, FormStateInterface $form_state) {
    unset($element['#name']);
    return $element;
  }

}
