<?php

namespace Drupal\commerce_promisepay\Plugin\Commerce\PaymentType;

use Drupal\commerce_payment\Plugin\Commerce\PaymentType\PaymentTypeBase;

/**
 * Provides the promisepay payment type.
 *
 * @CommercePaymentType(
 *   id = "payment_promisepay",
 *   label = @Translation("PromisePay"),
 *   workflow = "payment_promisepay",
 * )
 */
class PaymentPromisePay extends PaymentTypeBase {

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    return [];
  }

}
